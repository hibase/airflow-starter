data google_secret_manager_secret_version "airflow_local_sa" {
  provider = google.compute
  secret = "sa-airflow"
}

resource kubernetes_secret "airflow_local_gcp_sa" {
  metadata {
    name = "airflow-local-gcp-sa"
  }
  data = {
    basename(local.airflow_gcp_sa_credentials_path) = data.google_secret_manager_secret_version.airflow_local_sa.secret_data
  }
}
