module "postgres" {
  source = "../terraform/modules/kubernetes-postgres"
  providers = {
    kubernetes = kubernetes
    helm = helm
  }
}

output "postgres__username" {
  sensitive = true
  value = module.postgres.username
}

output "postgres__password" {
  sensitive = true
  value = module.postgres.password
}

output "postgres__superuser" {
  sensitive = true
  value = module.postgres.superuser
}

output "postgres__superuser_password" {
  sensitive = true
  value = module.postgres.superuser_password
}

output "postgres__database" {
  value = module.postgres.database
}

