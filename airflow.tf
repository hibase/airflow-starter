locals {
  airflow_gcp_sa_credentials_path = "/secrets/google_application_credentials.json"
  airflow_extra_volumes_and_volume_mounts = {
    "extraVolumes" = [
      {
        "name" = "local-repo"
        "hostPath" = {
          "path" = abspath(path.module)
          "type" = "Directory"
        }
      },
      {
        "name" = "local-gcp-sa"
        "secret" = {
          "secretName" = kubernetes_secret.airflow_local_gcp_sa.metadata[0].name
        }
      },
      {
        "name" = "variables"
        "secret" = {
          "secretName" = kubernetes_secret.airflow_variables.metadata[0].name
        }
      },
      {
        "name" = "connections"
        "secret" = {
          "secretName" = kubernetes_secret.airflow_connections.metadata[0].name
        }
      },
    ],
    "extraVolumeMounts" = [
      {
        "name" = "local-repo"
        "mountPath" = "/home/airflow/dags"
        "subPath" = "dags"
      },
      {
        "name" = "local-repo"
        "mountPath" = "/home/airflow/logs"
        "subPath" = "logs"
      },
      {
        "name" = "local-repo"
        "mountPath" = "/home/airflow/plugins"
        "subPath" = "plugins"
      },
      {
        "name" = "local-gcp-sa"
        "mountPath" = local.airflow_gcp_sa_credentials_path
        "subPath" = basename(local.airflow_gcp_sa_credentials_path)
      },
      {
        "name" = "variables"
        "mountPath" = "/home/airflow/${local.airflow_variables_filename}"
        "subPath" = local.airflow_variables_filename
      },
      {
        "name" = "connections"
        "mountPath" = "/home/airflow/${local.airflow_connections_filename}"
        "subPath" = local.airflow_connections_filename
      },
    ]
  }
}

module "airflow" {
  source = "../terraform/modules/kubernetes-airflow"
  providers = {
    kubernetes = kubernetes
    helm = helm
  }

  uid = var.airflow_uid
  gid = var.airflow_gid

  image_registry = "localhost:32000"
  image_name = "airflow"
  image_tag = "2.1.2r2"
  image_pull_policy = "Always"

  database_host = "postgresql.default.svc.cluster.local"
  database_user = module.postgres.username
  database_password = module.postgres.password
  database_name = module.postgres.database

  values = [
    yamlencode({
      "web" = merge(local.airflow_extra_volumes_and_volume_mounts, {
        "service" = {
           "serviceType" = "NodePort"
           "nodePort" = 30000
         }
      })
      "scheduler" = local.airflow_extra_volumes_and_volume_mounts
      "worker" = local.airflow_extra_volumes_and_volume_mounts
    })
  ]

  extra_env = {
    "GOOGLE_APPLICATION_CREDENTIALS" = local.airflow_gcp_sa_credentials_path
    "AIRFLOW__SECRETS__BACKEND" = "airflow.secrets.local_filesystem.LocalFilesystemBackend"
    "AIRFLOW__SECRETS__BACKEND_KWARGS" = jsonencode({
      "variables_file_path" = "/home/airflow/${local.airflow_variables_filename}"
      "connections_file_path" = "/home/airflow/${local.airflow_connections_filename}"
    })
    "AIRFLOW_MIN_FILE_PROCESS_INTERVAL" = "0"
    "AIRFLOW_DAG_DIR_LIST_INTERVAL" = "10"
    "AIRFLOW__WEBSERVER__WORKERS" = "2"
    "AIRFLOW_VAR_ENV" = "dev"
    "AIRFLOW_VAR_BIGQUERY_PII_STAGE_BUCKET" = "bigquery-stage-piis-3407ff"
    "AIRFLOW_CONN_POSTGRESQL_KEYBASE" = module.postgres.cluster_internal_superuser_uri
    "AIRFLOW_CONN_GOOGLE_CLOUD_PLATFORM" = "google-cloud-platform://?extra__google_cloud_platform__project=${data.google_project.non_piis.project_id}"
    "AIRFLOW_CONN_GOOGLE_CLOUD_DEFAULT" = "google-cloud-platform://?extra__google_cloud_platform__project=${data.google_project.non_piis.project_id}"
    "AIRFLOW_CONN_BIGQUERY_DEFAULT" = "google-cloud-platform://?extra__google_cloud_platform__project=${data.google_project.non_piis.project_id}"
    "AIRFLOW_CONN_GOOGLE_CLOUD_PLATFORM_PIIS" = "google-cloud-platform://?extra__google_cloud_platform__project=${data.google_project.piis.project_id}"
    "AIRFLOW_CONN_GOOGLE_CLOUD_PIIS" = "google-cloud-platform://?extra__google_cloud_platform__project=${data.google_project.piis.project_id}"
    "AIRFLOW_CONN_BIGQUERY_PIIS" = "google-cloud-platform://?extra__google_cloud_platform__project=${data.google_project.piis.project_id}"
    "AIRFLOW_CONN_CLOUDRUN_FB_CONVERSIONS_EXPORT" = "https://h5XU3cQ9DuqhfOiLBwH1:RFBPNrvbcjt7V50NDYox@fb-conversions-export-sjonmb2vfq-ey.a.run.app"
    "AIRFLOW__SMTP__SMTP_HOST" = ""
    "AIRFLOW__SMTP__SMTP_PORT" = ""
    "AIRFLOW__SMTP__SMTP_USER" = ""
    "AIRFLOW__SMTP__SMTP_MAIL_FROM" = ""
    # "AIRFLOW__SMTP__SMTP_PASSWORD" = data.google_secret_manager_secret_version.smtp_password.secret_data
    "AIRFLOW__SMTP__STARTTLS" = "True"
    "AIRFLOW__SMTP__SMTP_SSL" = "False"
  }
}

