from airflow.providers.google.cloud.hooks.gcs import GCSHook

def check_any_objects(bucket_name, path, *, gcp_conn_id='google_cloud_default'):
    """
    Check if any object exists in the bucket at the given path.
    Path may include a single '*' asterisk for glob-like behaviour.

    Returns True or False
    """
    for obj in glob_objects(bucket_name, path, gcp_conn_id=gcp_conn_id):
        return True
    return False

def glob_objects(bucket_name, path, *, gcp_conn_id='google_cloud_default'):
    """
    Returns an iterator for every object at path in the given bucket.
    Path may include a single '*' asterisk for glob-like behaviour.

    Returns iterator
    """
    hook = GCSHook(gcp_conn_id=gcp_conn_id)
    if path.count('*') == 1:
        prefix, suffix = path.split('*')
    else:
        prefix = path
        suffix = ''
    for filename in hook.list(bucket_name, prefix=prefix):
        if filename.endswith(suffix):
            yield filename

def get_blob(bucket_name, path):
    storage_client = GCSHook().get_conn()
    bucket = storage_client.bucket(bucket_name)
    return bucket.get_blob(path)

def stream_blob(blob, max_size=None):
    chunk_size = blob.chunk_size or 1000000
    total_size = max_size or blob.size
    remaining_bytes = max_size
    next = 0
    while remaining_bytes > 0:
        end = min(next + chunk_size, total_size)
        chunk = blob.download_as_bytes(start=next, end=end, raw_download=True)
        next = end + 1
        remaining_bytes = total_size - next
        yield chunk

def stream_first_line(bucket, path, max_size=1000000):
    buffer = b''
    first_line = None
    blob = get_blob(bucket, path)
    for chunk in stream_blob(blob, max_size):
        if b'\n' in chunk:
            lines = (buffer + chunk).replace(b'\r\n', b'\n').split(b'\n')
            first_line = lines[0]
            return first_line
        else:
            buffer += chunk
        if buffer > len(max_size):
            raise('Could not find first line, max size exceeded')
    return buffer

def stream_csv_header(bucket, path, **csv_opts):
    line = stream_first_line(bucket, path)
    return line.decode('utf-8').split(csv_opts.get('delimiter', ','))