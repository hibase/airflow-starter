import pendulum
import hashlib
from airflow.plugins_manager import AirflowPlugin

def ts(ti):
    execution_date = pendulum.instance(ti.execution_date)
    return execution_date.strftime('%Y-%m-%d %H:%M:%S')

def next_ts(ti):
    next_execution_date = pendulum.instance(ti.task.dag.following_schedule(ti.execution_date))
    return next_execution_date.strftime('%Y-%m-%d %H:%M:%S')

def tmp_table_name(ti):
    ti_string = ''.join([ti.dag_id, str(ti.execution_date)])
    return 'tmp_' + hashlib.md5(ti_string.encode('utf-8')).hexdigest()

class AirflowMacrosPlugin(AirflowPlugin):
    name = "bq"
    macros = [
        ts,
        next_ts,
        tmp_table_name,
    ]

