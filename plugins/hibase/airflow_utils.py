from os import environ
import re
from airflow import DAG
from airflow.models import Variable
import airflow.settings
from airflow.models import DagModel

AIRFLOW_ENV = Variable.get('env', default_var='default')

# Note: This is implemented in Airflow 2.0
# from airflow.utils.helpers import merge_dicts
def merge_dicts(dict1, dict2):
    """
    Merge two dicts recursively, returning new dict (input dict is not mutated).
    Lists are not concatenated. Items in dict2 overwrite those also found in dict1.
    """
    merged = dict1.copy()
    for k, v in dict2.items():
        if k in merged and isinstance(v, dict):
            merged[k] = merge_dicts(merged.get(k, {}), v)
        else:
            merged[k] = v
    return merged

def get_env():
    return AIRFLOW_ENV

def normalize_name(name):
    name = re.sub('[^a-zA-Z0-9]', '_', name)
    name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    name = re.sub('([a-z0-9])([A-Z])', r'\1_\2', name)
    name = re.sub('^_+', '', name)
    name = re.sub('_+', '_', name)
    name = re.sub('_$', '', name)
    return name.lower()

def gcs_path(dag_id, task_id, *paths):
    return '/'.join([dag_id, task_id, *paths])

def bq_dataset(dataset_name):
    return dataset_name

def bq_dataset_table(dataset_name, table_name):
    return '{}.{}'.format(bq_dataset(dataset_name), table_name)

def dynamic_dag(*, name, unit, **kwargs):
    dag_id = '{}.{}'.format(name, unit)
    return (dag_id, DAG(dag_id, **kwargs))

def unpause_dag(dag):
    """
    A way to programatically unpause a DAG.
    :param dag: DAG object
    :return: dag.is_paused is now False
    """
    session = airflow.settings.Session()
    try:
        qry = session.query(DagModel).filter(DagModel.dag_id == dag.dag_id)
        d = qry.first()
        d.is_paused = False
        session.commit()
    except:
        session.rollback()
    finally:
        session.close()
