import random
import string
from base64 import b64encode, b64decode
from Cryptodome.Cipher import AES
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.utils.decorators import apply_defaults

PUBLIC_REF_ALPHABET = string.ascii_letters + string.digits
def generate_public_ref():
    return ''.join(random.SystemRandom().choice(PUBLIC_REF_ALPHABET) for _ in range(10))

def encrypt(header, data, secret):
    header_b, data_b, secret_b = [ bytes(s, 'utf-8') for s in (header, data, secret) ]
    # Note: We want deterministic messages, therefore we set no nonce
    cipher = AES.new(secret_b, AES.MODE_SIV, nonce=None)
    cipher.update(header_b)
    enc, tag = cipher.encrypt_and_digest(data_b)
    values = [ b64encode(x).decode('utf-8') for x in (header_b, enc, tag) ]
    message = '.'.join(values)
    return message

def decrypt(datum, secret):
    header_b, enc_b, tag_b = [b64decode(s) for s in datum.split('.')]
    secret_b = secret
    if isinstance(secret, str):
        secret_b = bytes(secret, 'utf-8')
    cipher = AES.new(secret_b, AES.MODE_SIV, nonce=None)
    cipher.update(header_b)
    plain_b = cipher.decrypt_and_verify(enc_b, tag_b)
    return plain_b.decode('utf-8')

class PiiEncryptionMixin(object):

    @apply_defaults
    def __init__(self,
            *,
            pii_ref_name=None,
            pii_ref_column=None,
            pii_columns=[],
            keybase_conn_id='postgres_default',
            keybase_schema='public',
            keybase_table_name='secrets',
            **kwargs):
        super(PiiEncryptionMixin, self).__init__(**kwargs)
        self.pii_ref_name = pii_ref_name
        self.pii_ref_column = pii_ref_column
        self.pii_columns = pii_columns
        self.keybase_conn_id = keybase_conn_id
        self.keybase_schema = keybase_schema
        self.keybase_table_name = keybase_table_name

    def execute(self, context):
        self.pg_conn = PostgresHook(postgres_conn_id=self.keybase_conn_id).get_conn()
        try:
            self.ensure_postgres_schema()
            super(PiiEncryptionMixin, self).execute(context)
        finally:
            self.pg_conn.close()

    def ensure_postgres_schema(self):
        extension_ddl = "CREATE EXTENSION IF NOT EXISTS pgcrypto"
        schema_ddl = "CREATE SCHEMA IF NOT EXISTS {schema}".format(schema=self.keybase_schema)
        table_ddl = """
        CREATE TABLE IF NOT EXISTS {schema}.{table_name} (
            ref_name    text       NOT NULL,
            private_ref text       NOT NULL,
            public_ref  text       NOT NULL,
            secret      text       DEFAULT substring(regexp_replace(encode(gen_random_bytes(64), 'BASE64'), %(crlf)s, '', 'g') for 32) CHECK (length(secret) = 32),
            created_at  timestamp  NOT NULL DEFAULT now(),
            revoked_at  timestamp,
            PRIMARY KEY (ref_name, private_ref),
            CONSTRAINT unique_public_ref UNIQUE(ref_name, public_ref)
        )
        """.format(schema=self.keybase_schema, table_name=self.keybase_table_name)
        cursor = self.pg_conn.cursor()
        try:
            cursor.execute(extension_ddl)
            cursor.execute(schema_ddl)
            cursor.execute(table_ddl, {'crlf': '[\r\n]'})
            self.pg_conn.commit()
        finally:
            cursor.close()

    def transform_row(self, schema, row):
        row = self.encrypt_row(schema, row)
        return super(PiiEncryptionMixin, self).transform_row(schema, row)

    def encrypt_row(self, schema, row):
        if len(self.pii_columns) == 0 and self.pii_ref_column is None:
           return row

        public_ref, secret, is_revoked = self.get_or_create_secret(schema, row)
        # replace the private ref with the public ref
        ref_index = schema.index(self.pii_ref_column)
        row[ref_index] = public_ref

        # nothing to encrypt, only swapping a private reference to a public reference
        if len(self.pii_columns) == 0:
            return row

        # pii reference got revoked, mark all fields as revoked
        if is_revoked:
            return self.revoke_row(schema, row)

        for col_index, column_name in enumerate(schema):
            # column does not need to be encrypted
            if column_name not in self.pii_columns:
                continue
            # value is NULL
            if row[col_index] is None:
                continue
            # value is an empty string
            if row[col_index] == '':
                row[col_index] = '(empty)'
                continue
            # value must be encrypted, coerce to string
            data = str(row[col_index])
            # encrypt
            encrypted = encrypt(public_ref, data, secret)
            # replace
            row[col_index] = encrypted
        return row

    def get_or_create_secret(self, schema, row):
        retries = 2
        ref_index = schema.index(self.pii_ref_column)
        private_ref = row[ref_index]
        public_ref = generate_public_ref()
        sql_params = {'ref_name': self.pii_ref_name, 'private_ref': private_ref, 'public_ref': public_ref}
        cursor = self.pg_conn.cursor()
        result = None
        while True:
            select_sql = """
            SELECT public_ref, secret, revoked_at
            FROM {schema}.{table_name}
            WHERE ref_name = %(ref_name)s AND private_ref = %(private_ref)s::text
            """
            select_sql = select_sql.format(schema=self.keybase_schema, table_name=self.keybase_table_name)
            cursor.execute(select_sql, sql_params)
            result = cursor.fetchone()
            if result:
                break
            insert_sql = """
            INSERT INTO {schema}.{table_name}
                   (ref_name,     private_ref,     public_ref)
            VALUES (%(ref_name)s, %(private_ref)s, %(public_ref)s)
            RETURNING public_ref, secret, revoked_at
            """
            insert_sql = insert_sql.format(schema=self.keybase_schema, table_name=self.keybase_table_name)
            try:
                cursor.execute(insert_sql, sql_params)
            except psycopg2.IntegrityError:
                self.pg_conn.rollback()
                retries -= 1
                if retries > 0:
                    continue
                else:
                    raise
            else:
                result = cursor.fetchone()
                self.pg_conn.commit()
                break
        cursor.close()
        return (result[0], result[1], result[2] is not None)

    def revoke_row(schema, row):
        for col_index, column_name in enumerate(schema):
            if column_name not in self.pii_columns:
                continue
            row[col_index] = '(revoked)'
        return row

