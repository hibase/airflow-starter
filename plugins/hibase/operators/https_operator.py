import requests
from airflow.providers.http.operators.http import SimpleHttpOperator
from airflow.exceptions import AirflowException
from airflow.providers.http.operators.http import HttpHook
from airflow.utils.operator_helpers import make_kwargs_callable

class HttpsHook(HttpHook):
  def get_conn(self, headers):
    """
    Returns http session for use with requests. Supports https.
    """
    conn = self.get_connection(self.http_conn_id)
    session = requests.Session()

    if "://" in conn.host:
      self.base_url = conn.host
    elif conn.schema:
      self.base_url = conn.schema + "://" + conn.host
    elif conn.conn_type:  # https support
      self.base_url = conn.conn_type + "://" + conn.host
    else:
      # schema defaults to HTTP
      self.base_url = "http://" + conn.host
    if conn.port:
      self.base_url = self.base_url + ":" + str(conn.port) + "/"
    if conn.login:
      session.auth = (conn.login, conn.password)
    if headers:
      session.headers.update(headers)
    return session

class HttpsOperator(SimpleHttpOperator):
  def execute(self, context):
    http = HttpsHook(self.method, http_conn_id=self.http_conn_id)
    self.log.info("Calling HTTP method")
    response = http.run(self.endpoint,
                        self.data,
                        self.headers,
                        self.extra_options)
    if self.log_response:
      self.log.info(response.text)
    if self.response_check:
      kwargs_callable = make_kwargs_callable(self.response_check)
      if not kwargs_callable(response, **context):
        raise AirflowException("Response check returned False.")
    if self.response_filter:
      kwargs_callable = make_kwargs_callable(self.response_filter)
      return kwargs_callable(response, **context)
    return response.text
