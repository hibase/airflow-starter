import os
import re
import tempfile
import csv
import json

from airflow.configuration import conf
from airflow.exceptions import AirflowException
from airflow.models import BaseOperator
from airflow.sensors.base import BaseSensorOperator
from airflow.utils.decorators import apply_defaults
from airflow.providers.sftp.hooks.sftp import SFTPHook
from airflow.providers.google.cloud.hooks.gcs import GCSHook
from hibase.airflow_utils import normalize_name
from hibase.pii_encryption_mixin import PiiEncryptionMixin


def sftp_find_file(sftp_conn_id, directories, fileregexp, sort_descending):
    sftp_hook = SFTPHook(ftp_conn_id=sftp_conn_id)
    # Note: SFTPHook is suffering from the usual Airflow code insanity.
    #       They advertise the SSHHook connection settings (e.g. providing a private key file),
    #       but then deviate from that in the implementation of the SFTPHook. Pathetic.
    if sftp_hook.key_file.startswith('-----BEGIN OPENSSH PRIVATE KEY-----'):
        key_file = tempfile.NamedTemporaryFile('w', delete=True)
        key_file.write(sftp_hook.key_file)
        key_file.flush()
        sftp_hook.key_file = key_file.name

    regexp = re.compile(fileregexp)
    for directory in directories:
        remote_paths = sftp_hook.list_directory(path=directory)
        remote_paths.sort(reverse=sort_descending)
        for remote_path in remote_paths:
            basename = os.path.basename(remote_path)
            match = regexp.match(basename)
            if match:
                return os.path.join(directory, remote_path)
    return None

class SftpFindFile(BaseOperator):
    """
    Copy data from an SFTP server, unzip the contents and upload them to GCS.
    """

    template_fields = ('directories','fileregexp')
    template_ext = ('.txt',)

    @apply_defaults
    def __init__(self,
        *,
        sftp_conn_id,
        directories,
        fileregexp,
        sort_descending=False,
        **kwargs
    ):
        super(SftpFindFile, self).__init__(**kwargs)
        self.sftp_conn_id = sftp_conn_id
        self.directories = directories
        self.fileregexp = fileregexp
        self.sort_descending = sort_descending

    def execute(self, context):
        return sftp_find_file(
                self.sftp_conn_id,
                self.directories,
                self.fileregexp,
                self.sort_descending
                )

class SftpFindFileSensor(BaseSensorOperator):
    """
    Copy data from an SFTP server, unzip the contents and upload them to GCS.
    """

    template_fields = ('directories','fileregexp')
    template_ext = ('.txt',)

    @apply_defaults
    def __init__(self,
        *,
        sftp_conn_id,
        directories,
        fileregexp,
        sort_descending=False,
        **kwargs
    ):
        super(SftpFindFileSensor, self).__init__(**kwargs)
        self.sftp_conn_id = sftp_conn_id
        self.directories = directories
        self.fileregexp = fileregexp
        self.sort_descending = sort_descending

    def poke(self, context):
        result = sftp_find_file(
                   self.sftp_conn_id,
                   self.directories,
                   self.fileregexp,
                   self.sort_descending)
        self.xcom_push(context=context, key='return_value', value=result)
        return result is not None

