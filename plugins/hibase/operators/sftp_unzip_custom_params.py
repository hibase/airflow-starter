import os
import re
import tempfile
import zipfile
import csv
import orjson

from airflow.configuration import conf
from airflow.exceptions import AirflowException
from airflow.utils.decorators import apply_defaults
from airflow.providers.sftp.hooks.sftp import SFTPHook
from airflow.providers.google.cloud.hooks.gcs import GCSHook
from hibase.operators.sftp_unzip_csv_to_gcs import SftpUnzipCsvToGcs
from hibase.airflow_utils import normalize_name

class SftpUnzipCustomParams(SftpUnzipCsvToGcs):
    """
    Copy data from an SFTP server, unzip the contents, transform CSV to JSONL and upload them to GCS.
    """

    template_fields = ('remote_full_path','gcs_destination_path','gcs_schema_path')
    template_ext = ('.txt',)

    @apply_defaults
    def __init__(self,
        *,
        keep_columns=[],
        repeated_column_name='params',
        **kwargs):
        super(SftpUnzipCustomParams, self).__init__(**kwargs)
        self.keep_columns = set([normalize_name(col) for col in keep_columns])
        self.repeated_column_name = repeated_column_name

    def bq_schema_for_csv(self, csvpath, schema_hints, csv_opts):
        schema = {}
        schema[self.repeated_column_name] = {
            'name': self.repeated_column_name,
            'type': 'RECORD',
            'mode': 'REPEATED',
            'fields': [
                {'name': 'key', 'type': 'STRING'},
                {'name': 'original_key', 'type': 'STRING'},
                {'name': 'value', 'type': 'STRING'},
            ],
        }
        with open(csvpath, 'r') as file:
            csv_reader = csv.reader(file, **csv_opts)
            # Note: At the time we assume first row is a list of column headers
            for row in csv_reader:
                for col in row:
                    column_name = normalize_name(col)
                    if column_name not in self.keep_columns:
                        continue
                    schema[col] = {
                        'name': column_name,
                        'type': schema_hints.get(column_name, 'STRING'),
                        'mode': 'NULLABLE'
                    }
                break
        self.log.info('Acquired schema from {}: {}'.format(csvpath, schema))
        return list(schema.values())

    def bq_data_from_csv(self, csvpath, csv_opts):
        tmp_file = tempfile.NamedTemporaryFile(delete=False)
        header = None
        with open(csvpath, 'r') as file:
            csv_reader = csv.reader(file, **csv_opts)
            # Note: At the time we assume first row is a list of column headers
            for row in csv_reader:
                if header is None:
                    header = [col for col in row]
                    continue
                json = self.transform_row_to_json(header, row)
                tmp_file.write(orjson.dumps(json, option=orjson.OPT_APPEND_NEWLINE))
        tmp_file.flush()
        return tmp_file.name, 'application/json'

    def transform_row_to_json(self, header, row):
        json = {}
        json[self.repeated_column_name] = []
        for col_index in range(len(row)):
            column_name = header[col_index]
            normalized_column_name = normalize_name(column_name)
            if normalized_column_name in self.keep_columns:
                json[normalized_column_name] = row[col_index]
            else:
                nested = {'key': normalized_column_name, 'original_key': column_name, 'value': row[col_index]}
                json[self.repeated_column_name].append(nested)
        return json

