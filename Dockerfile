ARG PYTHON_VERSION=3.7.9
ARG BASE_LAYER=base-layer
ARG AIRFLOW_VERSION=2.1.2
ARG AIRFLOW_PIP_CONSTRAINTS_VERSION=3.7
ARG AIRFLOW_PIP_CONSTRAINT="https://raw.githubusercontent.com/apache/airflow/constraints-${AIRFLOW_VERSION}/constraints-${AIRFLOW_PIP_CONSTRAINTS_VERSION}.txt"
ARG PIP_VERSION=21.2.2
ARG UID=1000
ARG GID=1000

FROM python:${PYTHON_VERSION}-slim-buster AS base-layer
ARG PIP_VERSION
ARG AIRFLOW_PIP_CONSTRAINT

ENV \
  AIRFLOW_HOME=/home/airflow \
  PYTHONPATH=${PYTHONPATH:+${PYTHONPATH}:}${AIRFLOW_HOME} \
  DEBIAN_FRONTEND=noninteractive \
  LANGUAGE=C.UTF-8 LANG=C.UTF-8 LC_ALL=C.UTF-8 LC_CTYPE=C.UTF-8 LC_MESSAGES=C.UTF-8 \
  PIP_NO_CACHE_DIR="true"

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    apt-utils \
    curl \
    libmariadb3 \
    freetds-bin \
    gosu \
    libffi6 \
    libkrb5-3 \
    libpq5 \
    libsasl2-2 \
    libsasl2-modules \
    libssl1.1 \
    locales  \
    netcat \
    rsync \
    sasl2-bin \
    sudo \
    tini \
    gnupg1 \
  && apt-get autoremove -yqq --purge \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* \
  # Rust is required for some Python packages
  && (curl https://sh.rustup.rs -sSf | sh -s -- -y) \
  # MSSQL
  && (curl --silent --fail https://packages.microsoft.com/keys/microsoft.asc | apt-key add -) \
  && (curl --silent --fail https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list) \
  && apt-get update \
  && ACCEPT_EULA=Y apt-get install -y msodbcsql17 mssql-tools \
  # MSSQL has lower standards. Quite literally.
  && sed -e 's/MinProtocol = .*/MinProtocol = TLSv1.0/' -e 's/CipherString = .*/CipherString = DEFAULT@SECLEVEL=1/' -i /etc/ssl/openssl.cnf \
  # /MSSQL
  && pip install --upgrade pip==${PIP_VERSION}

FROM ${BASE_LAYER} AS airflow-layer
ARG AIRFLOW_VERSION
ARG AIRFLOW_PIP_CONSTRAINT

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    build-essential \
    default-libmysqlclient-dev \
    libffi-dev \
    libkrb5-dev \
    libpq-dev \
    libsasl2-dev \
    libssl-dev \
    unixodbc-dev \
    python3-dev \
  && pip install \
    apache-airflow==${AIRFLOW_VERSION} \
    --constraint ${AIRFLOW_PIP_CONSTRAINT} \
  && apt-get autoremove -yqq --purge \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY requirements.txt vendor/apache-airflow-providers-google-4.0.1.tar.gz /root/
RUN pip install -r /root/requirements.txt # --constraint ${AIRFLOW_PIP_CONSTRAINT}

FROM ${BASE_LAYER} AS airflow-user
ARG AIRFLOW_PIP_CONSTRAINT
ARG UID
ARG GID

COPY --from=airflow-layer /usr/local/lib/python3.7/site-packages /usr/local/lib/python3.7/site-packages
COPY --from=airflow-layer /usr/local/bin /usr/local/bin

RUN mkdir /home/airflow \
  && addgroup --gid ${UID} airflow \
  && adduser airflow --uid ${UID} --gid ${GID} --no-create-home --home /home/airflow \
  && chown -R airflow:airflow /home/airflow

USER airflow
WORKDIR /home/airflow

ENTRYPOINT ["/usr/bin/tini", "--"]
CMD ["airflow", "--help"]

