data google_container_registry_repository "default" {
  provider = google.compute
}

data google_project "compute" {
  provider = google.compute
}

data google_project "non_piis" {
  provider = google.non_piis
}

data google_project "piis" {
  provider = google.piis
}
