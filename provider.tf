terraform {
  required_version = "~>1.0.3"
  required_providers {
    google     = "~>3.47"
    kubernetes = "~>1.13.3"
    helm       = "~>1.3.2"
  }
}

provider kubernetes {
  config_context   = var.kubeconfig_context
  load_config_file = true
}

provider helm {
  kubernetes {
    config_context   = var.kubeconfig_context
    load_config_file = true
  }
}

# Note: this provider is deprecated, remove as soon everybody has re-done their setup
provider google {
  alias = "token_generator"
}

# Note: We have multiple providers with aliases to fetch resources from each project.
#       Make sure you use the correct provider alias, for example:
#
#         data google_project "compute" {
#           provider = google.compute
#         }

provider google {
  project = var.compute_project_id
  alias = "compute"
}

provider google {
  project = var.non_piis_project_id
  alias = "non_piis"
}

provider google {
  project = var.piis_project_id
  alias = "piis"
}
