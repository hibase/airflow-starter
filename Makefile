AIRFLOW_VERSION = 2.1.2
BUILD_REVISION ?= "r2"
LOCAL_TAG = localhost:32000/airflow:$(AIRFLOW_VERSION)$(BUILD_REVISION)
REMOTE_TAG = gcr.io/$(TF_VAR_compute_project_id)/airflow:$(AIRFLOW_VERSION)$(BUILD_REVISION)

build:
	docker build --build-arg UID=$$TF_VAR_airflow_uid --build-arg GID=$$TF_VAR_airflow_gid -t $(LOCAL_TAG) .
	-docker push $(LOCAL_TAG)

restart:
	kubectl delete po $$(kubectl get po -lcomponent=scheduler -ojsonpath={.items[0].metadata.name})
	kubectl delete po $$(kubectl get po -lcomponent=web -ojsonpath={.items[0].metadata.name})

release:
	docker build -t $(REMOTE_TAG) .
	docker push $(REMOTE_TAG)

reset:
	helm delete airflow
	terraform apply -auto-approve

