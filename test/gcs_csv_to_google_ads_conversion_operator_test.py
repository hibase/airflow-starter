import unittest
from datetime import datetime
from airflow import DAG
from airflow.models import TaskInstance

from pc.operators.gcs_csv_to_google_ads_conversion_operator import GCSCsvToGoogleAdsConversionOperator

class GCSCsvToGoogleAdsConversionOperatorTest(unittest.TestCase):
    # def test_execute_failure(self):
    #     dag = DAG(dag_id='test', start_date=datetime.now())
    #     task = GCSCsvToGoogleAdsConversionOperator(
    #             dag=dag,
    #             task_id='test',
    #             bucket_name='bigquery-stage-122a61',
    #             object_name='tmp/gads_export_7.csv',
    #             google_ads_conn_id='google_ads_pc',
    #             google_ads_load_from_env=False,
    #             customer_id='9149038812', # Main account
    #             # customer_id='4329834505', # Sub account
    #             # conversion_action_id='525443991', # This is taken from the CSV
    #             )
    #     ti = TaskInstance(task=task, execution_date=datetime.now())
    #     with self.assertRaises(SystemExit):
    #         task.execute(ti.get_template_context())


    def test_execute_success(self):
        dag = DAG(dag_id='test', start_date=datetime.now())
        task = GCSCsvToGoogleAdsConversionOperator(
                dag=dag,
                task_id='test',
                bucket_name='bigquery-stage-122a61',
                object_name='gads_conversions_export_poc/2021-07-22/*.csv',
                google_ads_conn_id='google_ads_pc',
                google_ads_load_from_env=False,
                customer_id='9149038812', # Main account
                # customer_id='4329834505', # Sub account
                # conversion_action_id='525443991', # This is taken from the CSV
                )
        ti = TaskInstance(task=task, execution_date=datetime.now())
        task.execute(ti.get_template_context())

if __name__ == '__main__':
    unittest.main()
