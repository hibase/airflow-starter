import unittest
import airflow
import xml.etree.ElementTree
from pc.xml_to_jsonl import xml_to_jsonl

class XmlToJSONLTest(unittest.TestCase):

    def test_xml_to_json(self):
        result = None
        columns = {
            "Retailstore_ID": ('string', "./Header/Retailstore_ID"),
            "Positionen": ('object', "./Positionen/Transaktionsposition"),
        }

        for row in xml_to_jsonl('tmp/test.xml', element_xpath="./Transaktion", columns=columns):
            result = row
            break

        expected = {
            "Retailstore_ID": "375",
            "Positionen": [
                {'Transaktionsposition': {

                        "Artikelverkauf": {
                            "Teilenummer": {'$':'PQCIYK2'}               }
                    },
                },
                {'Transaktionsposition': {
                        "Artikelverkauf": {
                            "Teilenummer": {'$':'ABCPQCIYK2'}               }
                    },
                }
            ]
        }
        self.assertEqual(expected, result)

    def test_xml_to_json_with_root_columns(self):
        result = None
        root_columns = {
            "BusinessDayDate": ('string', './BusinessDayDate'),
        }
        columns = {
            "Retailstore_ID": ('string', "./Header/Retailstore_ID"),
        }

        for row in xml_to_jsonl('tmp/test.xml', element_xpath="./Transaktion", root_columns=root_columns, columns=columns):
            result = row
            break

        expected = {
            "BusinessDayDate": "2021-02-01",
            "Retailstore_ID": "375",
        }
        self.assertEqual(expected, result)

if __name__ == '__main__':
    unittest.main()