import unittest
import airflow
from googl.gcs_helper import glob_objects
from pc.consts import BIGQUERY_STAGE_BUCKET
from pc.generic_csv_import import SOURCE_PATH

class CSVtoJsonlGCSOperator(unittest.TestCase):
    def test_glob_objects(self):
        for obj in glob_objects(BIGQUERY_STAGE_BUCKET, '{}/*/'.format(SOURCE_PATH)):
            import os
            print(os.path.basename(os.path.dirname(obj)))
            print(obj)

if __name__ == '__main__':
    unittest.main()
