import unittest
import airflow
from hibase.airflow_utils import normalize_name

class EmailshaTest(unittest.TestCase):

    def test_normalize_name(self):
        # normalizes dots, uppercase, CamelCase into sneak_case
        result = normalize_name('dbo.Dim_ProductHistorical_SCD2')
        self.assertEqual('dbo_dim_product_historical_scd2', result)
        # removes trailing underscores
        result = normalize_name('dbo.Dim_ProductHistorical_')
        self.assertEqual('dbo_dim_product_historical', result)
        # removes leading underscores
        result = normalize_name('#"SID"')
        self.assertEqual('sid', result)
        result = normalize_name('_sid')
        self.assertEqual('sid', result)


if __name__ == '__main__':
    unittest.main()


