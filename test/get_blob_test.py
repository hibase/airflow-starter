import unittest
from datetime import datetime
from airflow import DAG
from airflow.models import TaskInstance
from airflow.providers.google.cloud.hooks.gcs import GCSHook

def get_blob(bucket_name, path):
    storage_client = GCSHook().get_conn()
    bucket = storage_client.bucket(bucket_name)
    return bucket.get_blob(path)

def stream_blob(blob, max_size=None):
    chunk_size = blob.chunk_size or 1000000
    total_size = max_size or blob.size
    remaining_bytes = max_size
    next = 0
    while remaining_bytes > 0:
        end = min(next + chunk_size, total_size)
        chunk = blob.download_as_bytes(start=next, end=end, raw_download=True)
        next = end + 1
        remaining_bytes = total_size - next
        yield chunk

def stream_first_line(bucket, path, max_size=1000000):
    buffer = b''
    first_line = None
    blob = get_blob(bucket, path)
    for chunk in stream_blob(blob, max_size):
        if b'\n' in chunk:
            lines = (buffer + chunk).split(b'\n')
            first_line = lines[0]
            return first_line
        else:
            buffer += chunk
        if buffer > len(max_size):
            raise('Could not find first line, max size exceeded')
    return buffer

def stream_csv_header(bucket, path, **csv_opts):
    line = stream_first_line(bucket, path)
    return line.decode('utf-8').split(csv_opts.get('delimiter', ','))

class GetBlobTest(unittest.TestCase):
    def test_execute(self):
        hook = GCSHook()
        with hook.provide_file(bucket_name='bigquery-stage-122a61', object_name='tmp/gads_export.csv') as f:
          print(f.name)

    def test_stream(self):
        first_line = stream_csv_header('bigquery-stage-122a61', 'tmp/gads_export.csv')
        expected = ['gclid', 'conversion_name', 'conversion_date_time', 'external_attribution_model', 'external_attribution_credit', 'conversion_value', 'currency_code']
        self.assertEqual(expected, first_line)

if __name__ == '__main__':
    unittest.main()
