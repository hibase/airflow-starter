import unittest
from datetime import datetime
from airflow import DAG
from airflow.models import TaskInstance

from pc.operators.xml_to_jsonl_gcs import XMLtoJsonlGCSOperator


class XMLtoJsonlGCSOperatorTest(unittest.TestCase):
    def test_execute(self):
        dag = DAG(dag_id='test', start_date=datetime.now())
        columns = {
            "Retailstore_ID": ('string', "./Header/Retailstore_ID"),
            "Positionen": ('object', "./Positionen/Transaktionsposition"),
        }

        task = XMLtoJsonlGCSOperator(
                dag=dag,
                task_id='pos_xml_test',
                element_xpath='.//Transaktion',
                columns=columns,
                xml_bucket_name='bigquery-stage-122a61',
                xml_filename='temp/*.xml',
                schema_filename='tmp/pos_test_schema.json',
                target_bucket_name='bigquery-stage-122a61',
                target_filename='tmp/{basename}.jsonl'
                )
        ti = TaskInstance(task=task, execution_date=datetime.now())
        task.execute(ti.get_template_context())


    def test_execute_simple_file(self):
        dag = DAG(dag_id='test', start_date=datetime.now())
        columns = {
            "Retailstore_ID": ('string', "./Header/Retailstore_ID"),
            "Positionen": ('object', "./Positionen/Transaktionsposition"),
        }

        task = XMLtoJsonlGCSOperator(
                dag=dag,
                task_id='pos_xml_test',
                element_xpath='.//Transaktion',
                columns=columns,
                xml_bucket_name='bigquery-stage-122a61',
                xml_filename='pos_test.xml',
                schema_filename='tmp/pos_test_schema.json',
                target_bucket_name='bigquery-stage-122a61',
                target_filename='tmp/pos_test.jsonl'
                )
        ti = TaskInstance(task=task, execution_date=datetime.now())
        task.execute(ti.get_template_context())

if __name__ == '__main__':
    unittest.main()
