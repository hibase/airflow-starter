import unittest
from datetime import datetime
from airflow import DAG
from airflow.models import TaskInstance

from hibase.operators.bigquery_get_value_operator import BigQueryGetValueOperator

class BigQueryGetValueOperatorTest(unittest.TestCase):
    def test_execute(self):
        dag = DAG(dag_id='test', start_date=datetime.now())
        task = BigQueryGetValueOperator(
                dag=dag,
                task_id='test',
                sql="SELECT CAST('2020-01-01' AS DATE)"
                )
        ti = TaskInstance(task=task, execution_date=datetime.now())
        task.execute(ti.get_template_context())

    def test_execute_error(self):
        dag = DAG(dag_id='test', start_date=datetime.now())
        task = BigQueryGetValueOperator(
                dag=dag,
                task_id='test',
                sql="SELECT CAST('2020-01-01' AS FOOGARBL)",
                error_value='1899-01-01'
                )
        ti = TaskInstance(task=task, execution_date=datetime.now())
        task.execute(ti.get_template_context())

if __name__ == '__main__':
    unittest.main()


