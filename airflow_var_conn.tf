locals {
  airflow_variables_filename = "variables.json"
  airflow_connections_filename = "connections.json"
  secrets_manager_sources = [
    "af-var-bigquery_stage_bucket",
    "af-var-bigquery_ephemeral_export_bucket",
    "af-var-dataform_project_id",
    "af-var-env",
    "af-var-westfalia_export_bucket",
    "af-conn-dataform_default",
    "af-conn-google_ads_pc",
    "af-conn-mysql_kubi",
    "af-conn-mssql_crm_datastore",
    "af-conn-bigquery_fashion_id",
    # Add a connection or variable to import from Google Secret Manager here
  ]
}

  data google_secret_manager_secret_version "airflow_var_conn" {
    provider = google.compute
    for_each = toset(local.secrets_manager_sources)
    secret = each.value
  }

  resource kubernetes_secret "airflow_variables" {
    metadata {
      name = "airflow-vars"
    }

    data = {
      "${local.airflow_variables_filename}" = jsonencode(
        merge({
          "hello-world" = "Hello from live!"
        },
        {
          for key in local.secrets_manager_sources:
            replace(key, "af-var-", "") => data.google_secret_manager_secret_version.airflow_var_conn[key].secret_data
            if substr(key, 0, 6) == "af-var"
        })
      )
    }
  }

  resource kubernetes_secret "airflow_connections" {
    metadata {
      name = "airflow-conns"
    }

    data = {
      "${local.airflow_connections_filename}" = jsonencode(
        merge(
          {
            "airflow_db" = module.postgres.cluster_internal_superuser_uri
          },
          {
            for key in local.secrets_manager_sources:
              replace(key, "af-conn-", "") => data.google_secret_manager_secret_version.airflow_var_conn[key].secret_data
              if substr(key, 0, 7) == "af-conn"
        }
      )
    )
  }
}

