# Airflow

## Local setup

Involved technologies: Shell, Docker, Google Cloud SDK, Terraform.

> Note: This is not necessary with the developer VM.

### After cloning the repository on a fresh install

* Materialise the `.envrc` file

    ```
    $ cp .envrc.example .envrc
    $ direnv allow
    ```

* Install Airflow and its dependencies

    ```
    $ pip install --upgrade pip==20.2.4
    $ pip install apache-airflow==2.0.1
    $ pip install -r requirements.txt
    ```

* Initialize the Airflow database for testing

    ```
    $ airflow db init
    ```

* Run the dag integrity test to see if your local setup works

    ```
    $ python test/dag_integrity_test.py
    ```

### Setting up local Airflow cluster

* Build the container

    ```
    $ make build
    ```

* Initialise terraform

    ```
    $ terraform init
    ```

* Apply the terraform declarations

    ```
    $ terraform apply -auto-approve
    ```

* Check that the Airflow services are up and running

    ```
    $ kubectl get deploy,po,svc
    ```
    
    You can access the local Airflow installation at http://localhost:8080/

* Create a dummy admin user for local testing

    First enter a running container:

    ```
    $ kubectl exec -ti $(kubectl get po -lcomponent=web -o=jsonpath='{.items..metadata.name}') -- bash
    ```

    Inside the container, create a new user:

    ```
    > airflow users create --role Admin --firstname First --lastname Last --email admin@example.com --password test --username admin
    ```

## General instructions

### Accessing the Airflow web ui

Once you have started the Airflow cluster, you can open it at http://localhost:8080/.
The setup is configured to launch the service at a `NodePort` on port 8080.

### Adding connections or variables

Connections are managed by Google Secrets Manager, therefore you need the necessary privileged to
access the service account at the time of the Terraform setup.

> Note: Do not create or store secrets locally or in the repository. We use gcloud authentication for
a reason, and Google Secrets Manager is a convenient way to share secrets across development environments.

Login to GCP, navigate to "Security > Secrets Manager".

All secrets prefixed with `af-conn` and `af-var` are available to Airflow locally and in production.
Those are fetched without the Secrets Manager prefix, e.g.

A secret in `af-var-hello-world` containing the payload `Hello World` can be accessed in Airflow
using:

```python
from airflow.models.variable import Variable

Variable.get('hello-world')
# => returns 'Hello World', reading the payload from "af-var-hello-world" in the Google Secrets Manager.
```

### Following logs from Airflow

Logs are synchronised back from the containers to your local directory in `logs/`.
You can find logs for a specific DAG/task/run in `logs/<name of dag>/<name of task>/`.

### Accessing the Airflow metadata/Postgres database

You can `kubectl port-forward svc/postgresql 5432` to expose the Postgres metadata database to
your local computer.

The superuser username is `postgres` and the password can be obtained through Terraform:

```
$ terraform output postgres__superuser_password
```

The normal username and password can also be obtained through Terraform:

```
$ terraform output postgres__username
$ terraform output postgres__password
$ terraform output postgres__database
```

### Rebuilding the docker containers

There is a `Makefile` to simplify the process of rebuilding the images.

```
$ make build
```

### Restarting/cycling the Airflow pods

Sometimes you want to restart the Airflow pods, without re-creating the whole setup.
This is also reflected in the `Makefile`:

```
$ make restart
```

### Starting from scratch

If you want to redo the setup, you can re-create everything using Terraform.

> Note: You must also delete the Postgres metadata database storage before re-creating resources,
otherwise you will have the previous data.

To destroy the local setup, issue the following commands:

```
$ terraform destroy
$ kubectl delete pvc data-airflow-postgresql-0
```

To build the cluster up again, issue the following command:

```
$ terraform apply
```

## Local workflow

* Dags are to be found in the `dags/` directory in this repository.

    > Note: These are mounted directly into Airflow running on your local Kubernetes.

* Create a feature branch

* Work and test your changes in Airflow

    * Run the Airflow web ui

        ```
        $ kubectl port-forward svc/airflow 8080
        ```

    * Enter a shell running Airflow

        ```
        $ kubectl exec -ti $(kubectl get po -lapp.kubernetes.io/component=web -ojsonpath={.items[0].metadata.name})
        > cd /opt/bitnami/airflow
        ```

* Review the data

* Commit your changes to the feature branch

* Create a merge request

* Merge the peer reviewed merge request back into master.

    The code on production will be regularly updated from the master branch.

