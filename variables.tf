variable "kubeconfig_context" {
  type        = string
  default     = "docker-desktop"
  description = "The kubeconfig context to use for this setup."
}

variable "compute_project_id" {
  type        = string
  description = "The Google Cloud project id for the compute project"
}

variable "non_piis_project_id" {
  type        = string
  description = "The Google Cloud project id for the non-piis project"
}

variable "piis_project_id" {
  type        = string
  description = "The Google Cloud project id for the piis project"
}

variable "region" {
  type        = string
  default     = "europe-west4"
  description = "The Google Cloud project id to target"
}

variable "airflow_uid" {}

variable "airflow_gid" {}

